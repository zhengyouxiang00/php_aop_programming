<?php
namespace aop\example;

class ExampleThreeClass {

    /**
     * @before(ExampleMonitorClass, monitorParam, param[data])
     * @before(ExampleMonitorClass, monitorAny)
     * @after(ExampleMonitorClass, monitorParam, param[data]|return[object&success&message])
     * @interpret(ExampleMonitorClass, monitorAny)
     */
    public function returnAny($data) {
        $obj = new \stdClass();
        $obj->success = TRUE;
        $obj->message = __METHOD__ . ' test';
        $obj->data = $data;
        return $obj;
    }

}