<?php
namespace aop\example;

class ExampleOneClass {

    /**
     * @before(ExampleMonitorClass, monitorParam, param[data])
     * @before(ExampleMonitorClass, monitorAny)
     * @replace(ExampleMonitorClass, replaceParam)
     * @after(ExampleMonitorClass, monitorParam, param[data]|return[object&success&message])
     * @interpret(ExampleMonitorClass, monitorAny)
     */
    public function returnAny($data) {
        $obj = new \stdClass();
        $obj->success = TRUE;
        $obj->message = __METHOD__ . ' test';
        $obj->data = $data;
        return $obj;
    }
}