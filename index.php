<?php
/**
 * PHP VERSION >= 7.0
 * @author horsezone
 * @date 2017.11.14
 * 调用之前，可以参考本目录下的 README.md 文件
 */

define('FILE_EXT', '.php');
define('PATH_SEPERATOR', '/');
define('BASE_NAMESPACE', 'aop');
define('APP_DIR', __DIR__ . '/');
define('TMP_DIR', APP_DIR . 'tmp/');
define('BASE_DIR', APP_DIR . 'base/');

require_once BASE_DIR . 'autoload' . FILE_EXT;

//测试微框架性能代码片段
$method = isset($argv[1]) ? trim($argv[1]) : '';
$concret = new \aop\test\TestCenterService;
if (method_exists($concret, $method)) {
	call_user_func([$concret, $method]);
	exit;
}

//实例片段
$aop = require_once APP_DIR . 'example/aop_index' . FILE_EXT;

$res = $aop->_bind(\aop\example\ExampleOneClass::class, NULL, 'returnAny', ['only_namespace']);
print_r($res);

echo "\n";

$res = $aop->_bind(\aop\example\ExampleTwoClass::class, new \aop\example\ExampleTwoClass, 'returnAny', ['namespace_and_object']);
print_r($res);

echo "\n";

$res = $aop->_bind('', new \aop\example\ExampleThreeClass, 'returnAny', ['only_object']);
print_r($res);