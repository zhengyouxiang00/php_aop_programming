#php\_aop_programming
  
   
###**切面框架核心文件**
> ``AOPConstructor.php``  切面入口文件  
> ``core目录``            切面核心文件目录  
> ``config目录``           切面配置文件目录  
   
   
###**耗时测试文件**
> ``test目录``         测试核心文件目录  
> ``tmp目录``          测试文件临时目录   
> 如果要查看执行性能  
> 请先运行 ``test目录``下的``index.sh``  
    

###**效果演示**
> 请在终端直接使用``php index.php``文件查看效果  
> 然后查看当前目录下的``tmp目录``  
> 会多出``example_info``文件，点击查看内容，结构大体如下   
>  
> ``` 
> {  
>    "execute_flag" : "after",
>    "class_name" : "aop\\test\\ExampleOneClass",  
>    "method_name" : "runAny",  
>    "param_rule" : ["0, num","1, key"],  
>    "method_param" : ["param1","param2"],  
>    "method_result" : "aop\\test\\ExampleTestClass::run execute successfully"  
> }  
> ```
> 
> ``execute_flag`` 当前植入函数片段执行顺序标识。该字段一共会有两个值：before代表会在目标函数执行前执行，after标识会在目标函数执行以后再执行  
> ``class_name`` 目标函数所在类的全路径名称  
> ``method_name``  目标函数名称，即需要监控的函数名称  
> ``param_rule`` 参数规则   
> ``method_param`` 目标函数参数列表数组  
> ``method_result`` 目标函数执行以后的返回值。execute_flag为before时，该字段的值为NULL  
> 
> **备注**：字段含义摘自``config/CallbackOperationTrait.php``  

###**注释规则**
> **``@bofore``**表示在调用目标函数之前执行``@before``内的逻辑  
> **``@after``** 表示在调用目标函数之后执行``@after``内的逻辑  
> **``@bofore``**以及**``@after``**(两者无先后)必须位于**``@param``之前**，否则，``@before``以及``@after``内的逻辑都不会执行。  
> **``@before``**以及**``@after``**函数的**``第一个参数表示要横向调用的文件名称``**，而**``第二个参数表示前者内的方法``**。横向调用的文件**需先在``config/AOPAbstractConfig.php``中进行配置**   
> **``@param``函数**的**第一个参数表示参数位移**(第一个参数的位移是0)，**第二个参数表示参数的别名**(别名可以和变量名称相同，也可以不同)。  


###**AOPCONSTRUCTOR中的execute参数说明**
> **``bizName``** 切面文件映射的key，用于获取在``config/AOPAbstractConfig.php``中配置的横向调用文件的命名空间   
> **``abstract``** 需要绑定到容器中的类的路径名称，可以为空  
> **``concret``** 需要绑定到容器中的类的具体实例，如果上方已传入类的路径名称，可以不传入具体的实例，此时的参数请设置为``NULL``。  
> **``methodName``** 需要绑定到容器中的目标方法名称。不能为空   
> **``params``** 上方目标方法所需要的参数列表数组  
