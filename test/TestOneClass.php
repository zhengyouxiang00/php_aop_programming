<?php
namespace aop\test;

use \aop\test\MonitorService;

class TestOneClass {

    private $_monitor = NULL;

    public function __construct() {
        $this->_monitor = new MonitorService;
    }

    public function commonOne() {
        $this->_monitor->recordInfo();
        $this->_monitor->monitorInfo();
    }

    public function commonTwo() {
        $this->_monitor->recordInfo();
        $this->_monitor->monitorInfo();
    }

    public function commonThree() {
        $this->_monitor->recordInfo();
        $this->_monitor->monitorInfo();
    }

    public function commonFour() {
        $this->_monitor->recordInfo();
        $this->_monitor->monitorInfo();
    }

    /**
     * @before(MonitorService, monitorInfo)
     * @before(MonitorService, recordInfo)
     */
    public function aopOne() {
    }

    /**
     * @before(MonitorService, monitorInfo)
     * @before(MonitorService, recordInfo)
     */
    public function aopTwo() {
    }

    /**
     * @before(MonitorService, monitorInfo)
     * @before(MonitorService, recordInfo)
     */
    public function aopThree() {
    }

    /**
     * @before(MonitorService, monitorInfo)
     * @before(MonitorService, recordInfo)
     */
    public function aopFour() {
    }
}