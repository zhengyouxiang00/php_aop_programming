<?php
$filename_array = [
    'aop_same_object_one_method_once' => '切面调用-方法执行一次',
    'aop_same_object_one_method_twice' => '切面调用-同一对象中相同方法被执行两次',
    'aop_same_object_one_method_third' => '切面调用-同一对象中相同方法被执行三次',
    'aop_same_object_one_method_fourth' => '切面调用-同一对象中相同方法被执行四次',
    'aop_same_object_two_different_method' => '切面调用-执行同一个对象中两个不同的方法',
    'aop_same_object_three_different_method' => '切面调用-执行同一个对象中三个不同的方法',
    'aop_same_object_four_different_method' => '切面调用-执行同一个对象中四个不同的方法',
    'aop_two_object_one_method' => '切面调用-执行两个不同对象中的一个方法',
    'aop_three_object_one_method' => '切面调用-执行三个不同对象中的一个方法',
    'aop_four_object_one_method' => '切面调用-执行四个不同对象中的一个方法',
    'common_same_object_one_method_once' => '直接调用-方法执行一次',
    'common_same_object_one_method_twice' => '直接调用-同一对象中相同方法被执行两次',
    'common_same_object_one_method_third' => '直接调用-同一对象中相同方法被执行三次',
    'common_same_object_one_method_fourth' => '直接调用-同一对象中相同方法被执行四次',
    'common_same_object_two_different_method' => '直接调用-执行同一个对象中两个不同的方法',
    'common_same_object_three_different_method' => '直接调用-执行同一个对象中三个不同的方法',
    'common_same_object_four_different_method' => '直接调用-执行同一个对象中四个不同的方法',
    'common_two_object_one_method' => '直接调用-执行两个不同对象中的一个方法',
    'common_three_object_one_method' => '直接调用-执行三个不同对象中的一个方法',
    'common_four_object_one_method' => '直接调用-执行四个不同对象中的一个方法',
];

foreach ($filename_array as $_file => $_name) {
	$filepath = '../tmp/' . $_file;
    if (!file_exists($filepath)) {
    	continue;
    }
    $count = 0;
    $totalTime = 0;
    foreach (explode("\n", file_get_contents($filepath)) as $_time) {
    	if (!is_numeric($_time)) {
    		continue;
    	}
    	$count++;
    	$totalTime += $_time;
    }
    if ($count <= 0) {
    	continue;
    }
    echo $_name . '的平均耗时：' . $totalTime / $count . "微秒\n";
}